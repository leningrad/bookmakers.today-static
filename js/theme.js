jQuery(function($) {

	var topSwiper = new Swiper ('#bmt-top-swiper', {
        navigation: {
			nextEl: '#bmt-top-swiper-next',
			prevEl: '#bmt-top-swiper-prev',
		},
	});

	var guessSwiper = new Swiper ('.bmt-rating__guess-container', {
        navigation: {
			nextEl: '.bmt-rating__guess-next',
			prevEl: '.bmt-rating__guess-prev',
		},
	});

	var navSwiper = [];
	$('.bmt-nav-swiper').each(function(ind, el) {
		var $container = $(el).find('.swiper-container');

		new Swiper ($container, {
			slidesPerView: 'auto',
			slideActiveClass: 'active',
	        navigation: {
				nextEl: $(el).find('.bmt-nav-swiper-next'),
				prevEl: $(el).find('.bmt-nav-swiper-prev'),
			},
		});
	});

	var verticalSwiper = new Swiper('.bmt-news-swiper__container', {
		direction: 'vertical',
		pagination: {
			el: '.swiper-pagination',
			dynamicMainBullets: 1,
			dynamicBullets: true,
			clickable: true
		},
		autoplay: {
			delay: 5000
		},
	});

	var articleSwiper = new Swiper('.bmt-article-swiper__container', {
		direction: 'vertical',
		pagination: {
			el: '.bmt-article-swiper__pagination',
			dynamicMainBullets: 1,
			dynamicBullets: true,
			clickable: true
		},
		autoplay: {
			delay: 5000
		},
	});
	

	var matches__tab = {
		container:     $('#matches__tab'),
		matches__nav:  $('#matches__tab').find('.bmt-nav-swiper'),

		init: function() {
			matches__tab.active__tab('all');
			matches__tab.clickHandler();
		},
		active__tab: function(category) {
			var items = matches__tab.container.find('.bmt-matches-aside__item');

			items.removeClass('hidden');

			if (category != 'all') {
				var nowDate = new Date();

				items.each(function(ind, el) {
					var date = new Date($(el).find('.bmt-matches-aside__item-date span').data('date'));

					if (category == 'immediate') {
						if (nowDate > date) {
							$(el).addClass('hidden');
						}
					}
					if (category == 'completed') {
						if (nowDate < date) {
							$(el).addClass('hidden');
						}
					}
				});
			}

			matches__tab.matches__nav.find('.swiper-slide').removeClass('active');
			matches__tab.matches__nav.find('a[data-category="' + category + '"]').parents('.swiper-slide').addClass('active');
		},
		clickHandler: function() {
			matches__tab.matches__nav.on('click', '.swiper-slide a', function() {
				var $this = $(this),
					category = $this.data('category');

				matches__tab.active__tab(category);
				return false;
			});
		}
	};
	matches__tab.init();

	$('.bmt-calculator').on('click', '.uk-button-group li a', function() {
		var $this = $(this);

		$this.parents('.uk-button-group').find('li').removeClass('uk-active');
		$this.parents('li').addClass('uk-active');

		return false;
	});

	$('.bmt-rating__title').click(function() {
		var w = $(window).width(),
			$this = $(this);
		if (w < 960) {
			if ($this.parents('.bmt-rating').hasClass('open')) {
				$this.parents('.bmt-rating').removeClass('open');
			}
			else {
				$this.parents('.bmt-rating').addClass('open');
			}
		}
	});

});
